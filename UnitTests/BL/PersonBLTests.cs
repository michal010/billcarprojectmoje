﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using System;

namespace Billcar.BL.Tests
{
    [TestClass()]
    public class PersonBLTests
    {
        private DatabaseController database;

        public PersonBLTests()
        {
            database = new DatabaseController();
        }

        //[TestMethod()]
        //public void PersonTests()
        //{
        //    AuthBL backend = new AuthBL();
        //    PersonBL pBL = new PersonBL();
        //    Person MyUser = new Person();
        //    Person MyUser2 = new Person() { Email = "abc", Password = "abc" };
        //    try
        //    {
        //        MyUser = backend.UserLogIn("abc", "abc");
        //        Console.WriteLine(MyUser.Id + " ," + MyUser.FirstName + " ," + MyUser.LastName + " ," + MyUser.Section);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Nie można się zalogować");
        //    }
        //    try
        //    {
        //        MyUser2 = backend.UserLogIn(MyUser);
        //        Console.WriteLine(MyUser.Id + " ," + MyUser.FirstName + " ," + MyUser.LastName + " ," + MyUser.Section);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Nie można się zalogować");
        //    }
        //    if (backend.IfUserExist(MyUser2))
        //        Console.WriteLine("Użytkownik istnieje w bazie");
        //    else
        //        Console.WriteLine("Użytkownik nie istnieje w bazie");

        //    try
        //    {
        //        Person MyUserReg = new Person() { FirstName = "testDodniaPassword", LastName = "Kowalski", Email = "testMail@test.com", Salt = "test",Password = "Test123$", Section = 1 };
        //        if (backend.UserRegister(MyUserReg))
        //            Console.WriteLine("Zarejestrowano pomyślnie!");
        //        else
        //            Console.WriteLine("Nie można się zarejestrować!");
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Wystąpił błąd podczas rejestracji");
        //    }

        //    //Person MyUser3 = new Person() {  Id=6, FirstName = "testDodawania", LastName = "Kowalski", Email = "testMail", Password = "kowalski123", Section = 1 };
        //    //Console.WriteLine(pBL.UpdatePerson(MyUser3));
        //    try
        //    {
        //        Person os = pBL.Read(1);
        //        Console.WriteLine(os.Id + " ," + os.FirstName + " ," + os.LastName + " ," + os.Section);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("osoba nie istnieje w bd");
        //    }

        //}
        //[TestMethod()]
        //public void SectionTests()
        //{
        //    SectionBL sbL = new SectionBL();
        //    Section sCreate = new Section() { CityName = "Ostróda" };
        //    sbL.Create(sCreate);

        //    Section sRead = sbL.Read(2);
        //    Console.WriteLine(sRead.CityName);

        //    Section sUpdate = new Section() { Id = 5, CityName = "Zmiana" };
        //    sbL.Update(sUpdate);

        //    Section sDelete = sbL.Read(3);
        //    sbL.Delete(sDelete);

        //}
        //[TestMethod()]
        //public void PosterTests()
        //{
        //    PosterBL pBL = new PosterBL();
        //    Poster pCreate = new Poster() { AuthorID = 1,TransportDate = "13:00:00 20/04/2017", From = "Stary Dwór", To = "Billenium Olsztyn", Direction = true, TransportMaxSize = 2,TransportCurrSize = 2 };
        //    pBL.Create(pCreate);

        //    Poster pRead = pBL.Read(1);
        //    Console.WriteLine(pRead.ToString());

        //    Poster pUpdate = pBL.Read(1);
        //    pUpdate.From = "Plac Roosevelta";
        //    pBL.Update(pUpdate);

        //    pBL.Delete(pBL.Read(2));
        //}
        //[TestMethod()]
        //public void GetPostersByPersonIdTests()
        //{
        //    PosterBL pBL = new PosterBL();
        //    try
        //    {
        //        foreach (Poster post in pBL.GetPostersByPerson(new Person() { Id = 1 }))
        //            Console.WriteLine("Poster id: " + post.Id + " Author id: " + post.AuthorID + " Direction: " + post.Direction + " From: " + post.From + " To: " + post.To + "Transport date: " + post.TransportDate + "CurrentSize: " + post.TransportCurrSize + "MaxSize: " + post.TransportMaxSize);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Błąd!");
        //    }
        //}
        //[TestMethod()]
        //public void Hashing()
        //{
        //    SecurityBL sBL = new SecurityBL();
        //    AuthBL aBL = new AuthBL();
        //    Person p = new Person() { FirstName = "Admin", LastName = "Admin", Email = "a@a.com", Salt = "test", Password = "Admin123$", Section = 1, Flags = true };

        //    //p.Salt = sBL.CreateSalt(32);
        //    //p.Password = sBL.GenerateSHA256Hash(p.Password, p.Salt);
        //    //Console.WriteLine(p.Salt);
        //    //Console.WriteLine(p.Salt.Length);
        //    //Console.WriteLine(p.Password);
        //    //Console.WriteLine(p.Password.Length);

        //    try
        //    {
        //        aBL.UserRegister(p);
        //        Console.WriteLine(p.Password);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Błąd rejestracji!");
        //    }

        //    try
        //    {
        //        Person pLogin = new Person() { Email = "a@a.com", Password = "Admin123$" };
        //        pLogin = aBL.UserLogIn(pLogin);
        //        Console.WriteLine(pLogin.FirstName + " " + pLogin.LastName);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Błąd logowania");
        //    }

        //}
        [TestMethod()]
        public void LittleTests()
        {
            SectionBL sBL = new SectionBL();
            Console.WriteLine(sBL.Create(new Section() { CityName = "Ostróda" }).Information);

            //AuthBL aBL = new AuthBL();
            //MailBL mBL = new MailBL();
            //Person reg = new Person() { FirstName = "Mich", LastName = "Pop", Email = "michael010pl@gmail.com", Password = "Test123$", Section = 1 };
            //aBL.UserRegister(reg);
            //Mail m = new Mail() { ToPersonID = 5, Subject = "Testowy subject", Body = "Testowe body" };
            //mBL.Create(m);
            //mBL.SendMails();
            //Person p = new Person(){ FirstName = "User", LastName = "User", Email = "u@u.com", Salt = "test", Password = "User123$", Section = 1, Flags = false };
            //ValidationResponse resp = aBL.UserRegister(p);
            //Console.Write(resp.Successful + " " + resp.Information);

        }
    }
}