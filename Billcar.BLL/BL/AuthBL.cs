﻿using System.Collections.Generic;
using System.Linq;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using System.Text.RegularExpressions;

namespace Billcar.BL
{
    public class AuthBL
    {
         private readonly DatabaseController _db;

        public AuthBL()
        {
            // Łączenie do warstwy bazy danych
            _db = new DatabaseController();
        }

        public List<Section> GetSections()
        {
            List<Section> sections = _db.GetSectionsDB();
            return sections;
        }

        //tokens
        public bool createToken(Token t)
        {
            return _db.TokenCreate(t);
        }
        public Token returnToken(Token t)
        {
            return _db.ReturnToken(t);
        }
        public bool RemoveToken(Token t)
        {
            return _db.TokemRemove(t);
        }
        public bool CheckToken(Token t)
        {
            return _db.TokenCheck(t);
        }


        public Person UserLogIn(string email, string password)
        {
            return _db.GetPersonByEmailPassword(email, password);
        }

        public Person UserLogIn(Person p)
        {
            SecurityBL sBL = new SecurityBL();
            Person comparePerson = _db.GetPersonByEmail(p.Email);
            if (comparePerson == null) return null;
            if (sBL.GenerateSHA256Hash(p.Password, comparePerson.Salt) == comparePerson.Password)
            {
                return comparePerson;
            }
            else
            {
                return new Person();
            }


        }
        public ValidationResponse UserRegister(Person p)
        {
            SecurityBL sBL = new SecurityBL();
            ValidationResponse response = new ValidationResponse();
            response = FirstNameValid(p.FirstName);
            if (!response.Successful) return response;
            response = LastNameValid(p.LastName);
            if (!response.Successful) return response;
            response = EmailValid(p.Email);
            if (!response.Successful) return response;
            response = PasswordValid(p.Password);
            if (!response.Successful) return response;
            p.Salt = sBL.CreateSalt(32);
            p.Password = sBL.GenerateSHA256Hash(p.Password, p.Salt);
            response.Successful = _db.PersonCreate(p);
            response.Information = response.Successful ? "Success!" : "Error with database!";
            return response;

            //if (!FirstNameValid(p.FirstName) || !LastNameValid(p.LastName) || !EmailValid(p.Email) || !PasswordValid(p.Password))
            //    return false;
            //p.Salt = sBL.CreateSalt(32);
            //p.Password = sBL.GenerateSHA256Hash(p.Password, p.Salt);
            //return db.PersonCreate(p);
        }

        public ValidationResponse IfUserExist(Person p)
        {
            ValidationResponse response = new ValidationResponse();
            Person person = _db.GetPersonByEmail(p.Email);
            if (person != null)
            {
                response.Information = "User exists in database";
                response.Successful = true;
            }   
            else
            {
                response.Information = "User does not exists in database";
                response.Successful = false;
            }
            return response;
        }



        //pseudo walidacja.
        public ValidationResponse FirstNameValid(string fName)
        {
            ValidationResponse response;
            if (fName.Length > 30)
            {
                response = new ValidationResponse(false, "First name is too long!");
            }
            else if(!Regex.IsMatch(fName, @"^[a-zA-Z]+$"))
            {
                response = new ValidationResponse(false, "First name contains not allowed characters!");
            }
            else
            {
                response = new ValidationResponse(true);
            }
            return response;
        }

        public ValidationResponse LastNameValid(string lName)
        {
            ValidationResponse response;
            if (lName.Length > 30)
            {
                response = new ValidationResponse(false, "Last name is too long!");
            }
            else if(!Regex.IsMatch(lName, @"^[a-zA-Z]+$"))
            {
                response = new ValidationResponse(false, "Last name contains not allowed characters!");
            }
            else
            {
                response = new ValidationResponse(true);
            }
            return response;
        }

        public ValidationResponse EmailValid(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            ValidationResponse response;
            if (!match.Success)
            {
                response = new ValidationResponse(false, "Email adress is not in correct format!");
            }
            else if (_db.GetPersonByEmail(email) != null)
            {
                response = new ValidationResponse(false, "This email adress is already taken!");
            }
            else
                response = new ValidationResponse(true);

            return response;
        }

        public ValidationResponse PasswordValid(string pass)
        {
            ValidationResponse response;
            if (pass.Length > 7 &&
                pass.Any(char.IsUpper) &&
                pass.Any(char.IsLower) &&
                pass.Any(char.IsDigit) &&
                pass.Any(c => char.IsLetterOrDigit(c)==false))
                response = new ValidationResponse(true);
            else
                response = new ValidationResponse(false, "Password must contain atleast one upper character, one lower character, one number, one special character and be atleast 8 characters long.");

            return response;
        }
    }
}