﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using static Billcar.DAL.Context.PersonContext;

namespace Billcar.BL
{
    public class PosterBL
    {
        private readonly DatabaseController _db;
        public PosterBL()
        {
            _db = new DatabaseController();
        }
        public List<Poster> GetPosters()
        {
            List<Poster> posters = _db.GetPostersDB();
            return posters;
        }
        public List<Poster> GetPostersByPerson(Person p)
        {
            List<Poster> posters = _db.GetPostersByPerson(p);
            return posters;
        }
        public bool Create(Poster pstr)
        {
            return _db.PosterCreate(pstr);
        }
        public Poster Read(int id)
        {
            return _db.PosterRead(id);
        }
        public bool Update(Poster pstr)
        {
            return _db.PosterUpdate(pstr);
        }
        public bool Delete(Poster pstr)
        {
            return _db.PosterDelete(pstr);
        }
    }
}