﻿using Billcar.Core.Models;
using Billcar.DAL.DB;

namespace Billcar.BL
{
    public class SectionBL
    {
        private readonly DatabaseController _db;
        public SectionBL()
        {
            _db = new DatabaseController();
        }


        public ValidationResponse Create(Section s)
        {
            ValidationResponse response;
            response = CityValid(s.CityName);
            if(response.Successful)
            {
                if (_db.SectionCreate(s))
                    response = new ValidationResponse(true, "Success!");
                else
                    response = new ValidationResponse(false, "Error with database!");
            }
            return response;
        }
        public Section Read(int id)
        {
            return _db.SectionRead(id);
        }
        public bool Update(Section s)
        {
            return _db.SectionUpdate(s);
        }
        public ValidationResponse Delete(Section s)
        {
            if(s.Person.Count>0)
                return new ValidationResponse(false, "Oddział ma przypisanych użytkowników.");
            if(_db.SectionDelete(s)==false)
                return new ValidationResponse(false, "Nie udało się usunąć oddziału.");
            return new ValidationResponse(true);

        }

        public ValidationResponse CityValid(string city)
        {
            ValidationResponse response;
            if (_db.GetPosterByCity(city) != null)
            {
                response = new ValidationResponse(false, "Section with that city name already exists!");
            }
            else
            {
                response = new ValidationResponse(true);
            }
            return response;
        }




        }
}