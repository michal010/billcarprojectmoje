﻿using System.Net;
using System.Net.Mail;
using System.Text;

namespace Billcar.BL
{
    public class EmailSender
    {
        readonly SmtpClient _client;
        private readonly MailMessage _msg;
        
        public EmailSender(string to, string subject, string body)
        {
            var login = new NetworkCredential("billcar.email.sender", "BillcarOLNProject990");


            _client = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = login,
                EnableSsl = true
            };

            _msg = new MailMessage { From = new MailAddress("billcar.email.sender@gmail.com", "BillCarBot", Encoding.UTF8) };
            _msg.To.Add(new MailAddress(to));
            _msg.Subject = subject;
            _msg.Body = body;
            _msg.BodyEncoding = Encoding.UTF8;
            _msg.IsBodyHtml = true;
            _msg.Priority = MailPriority.Normal;
            _msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            
        }

        public void Send()
        {
            //client.SendAsync(msg, "Sending...");
            
            _client.Send(_msg);
        }
    }
}