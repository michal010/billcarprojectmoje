﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using static Billcar.DAL.Context.PersonContext;

namespace Billcar.BL
{
    public class PersonBL
    {
        private readonly DatabaseController _db;
        public PersonBL()
        {
            _db = new DatabaseController();
        }
        public List<Person> GetPeople()
        {
            List<Person> people = _db.GetPeopleDB();
            return people;
        }
        public bool Create(Person p)
        {
            return _db.PersonCreate(p);
        }
        public Person Read(int id)
        {
            return _db.PersonRead(id);
        }
        public bool Update(Person p)
        {
            return _db.PersonUpdate(p);
        }
        public bool Delete(Person p)
        {
            return _db.PersonDelete(p);
        }


    }
}