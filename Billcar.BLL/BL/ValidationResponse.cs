﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billcar.BL
{
    public class ValidationResponse
    {
        public bool Successful { get; set; }
        public string Information { get; set; }

        public ValidationResponse()
        {

        }

        public ValidationResponse(bool succ)
        {
            Successful = succ;
        }

        public ValidationResponse(bool succ, string info)
        {
            Successful = succ;
            Information = info;
        }
    }
}