﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Billcar.Core.Models;

namespace Billcar.DAL.Context
{
        public partial class BazaDanychMW : DbContext
        {
        // Plik bazy danych ma nazwę BazaDanychMW
        // Nazwałem więc kontekst połączenia tak samo z definicją tabel
        // W ten sposób chyba ułatwi co do czego.
        public BazaDanychMW()
                : base("name=BazaDanychMW")
            {
                // -------
                // JSON loop disable for debug app AND allow connection tables in backend
                string test = AppDomain.CurrentDomain.BaseDirectory;
                if(test.IndexOf("TestBackend")<=0)
                this.Configuration.LazyLoadingEnabled = false;  // Ważne, inaczej powstanie nieskończona pętla JSON...
                // ------
            }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                throw new UnintentionalCodeFirstException();
            }

            // Nasza funkcja która mówi, że funkcja Persons zwraca z bazy danych klasę typu Person [tzn bazę danych tego typu]
            public virtual DbSet<Person> Persons { get; set; }
            public virtual DbSet<Section> Sections { get; set; }
        }
}