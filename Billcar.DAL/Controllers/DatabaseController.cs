﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using static Billcar.DAL.Context.PersonContext;


namespace Billcar.DAL.DB
{
    public class DatabaseController
    {

        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        private BazaDanychMWEntities1 db = new BazaDanychMWEntities1();
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Warstwa polaczenia z baza danych tj. wywolujemy ta klase aby pobrac dane do jakiejs zmiennej z bazy
        /// np. metodą GetPeopleDB
        /// 
        /// </summary>
        /// 

        public DatabaseController()
        {
            // Sciezka projektu... niestety my chcemy sciezke solucji!
            var dir = AppDomain.CurrentDomain.BaseDirectory.ToString();
            // Cofamy się z projektu
            dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\"));
            if (dir.IndexOf("TestBackend") > 0)
            {
                dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\..\\"));
            }
            // Bierzemy dodajemy do solucji sciezke do bazy danych i utworzomy ze to tutaj jest folder danych
            dir = dir + "Billcar.DAL\\App_Data\\";
            //.SetData("DataDirectory", AppDomain.CurrentDomain.ToString());
            AppDomain.CurrentDomain.SetData("DataDirectory", dir);
        }

        //Ponizej metody ktore mozemy wykonywac z bazy danych tj. pobierac rekordy, dodawac nowe itp.

        public List<Person> GetPeopleDB()
        {
            // Pobieramy z bazy danych Persons i zapisujemy to do listy
            List<Person> people = db.Persons.ToList();
            // Zwracamy dane i nic więcej
            return people;
        }

        public List<Test> GetTestDB()
        {
            List<Test> tests = db.Test.ToList();
            return tests;
        }
    }
    
}