﻿using Billcar.Core.Models;
using Billcar.DAL.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

using static Billcar.DAL.Context.BazaDanychMW;


namespace Billcar.DAL.DB
{
    public class VirtualDB
    {
        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        internal BazaDanychMW db = new BazaDanychMW();
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Zwraca wszystkie osoby z tabelki Person!
        /// TEST
        /// </summary>
        public VirtualDB()
        {
                // Sciezka projektu... niestety my chcemy sciezke solucji!
                var dir = AppDomain.CurrentDomain.BaseDirectory.ToString();
                // Cofamy się z projektu
                dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\"));
                if (dir.IndexOf("TestBackend") > 0)
                {
                    dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\..\\"));
                }
                // Bierzemy dodajemy do solucji sciezke do bazy danych i utworzomy ze to tutaj jest folder danych
                dir = dir + "Billcar.DAL\\App_Data\\";
                //.SetData("DataDirectory", AppDomain.CurrentDomain.ToString());
                AppDomain.CurrentDomain.SetData("DataDirectory", dir);
        }
    }
}