'use strict';

angular.module('sbAdminApp')
    .controller("InfoPosters", ['$http', '$scope', '$cookies', function ($http, $scope, $cookies) {
        console.log("InfoPosters");
            //var user = {
            //    Id: 2
            //}

            $http({
                method: "POST", url: "/api/PostersCountInfo", dataType: 'json',
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function (response, status) {
                console.log("SHOW:::");
                console.log(response.data);
                $scope.systemPosters = response.data[0];
                $scope.sectionPosters = response.data[1];
                $scope.userPosters = response.data[2];
            }, function errorCallback(response) {
                console.log("ERROR");
                console.log(response);
            });
            //$http({
            //    method: "GET", url: "/api/GetPostersPerson", dataType: 'json',
            //    headers: {
            //        "Content-Type": "application/json"
            //    },
            //    params: user
            //}).then(function (response, status) {
            //    $scope.userPosters = response.data;
            //}, function errorCallback(response) {
            //    console.log("ERROR");
            //    console.log(response);
            //});
}]);
