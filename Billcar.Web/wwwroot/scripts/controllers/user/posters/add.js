"use strict";
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module("sbAdminApp")
app.service("AddPosterService", function ($http) {
    

    this.addPoster = function (addPosterForm) {
        $http({
            method: "POST", url: "/api/AddPoster", dataType: "json",
            headers: { "Content-Type": "application/json" },
            data: { poster: addPosterForm }
        }).then(function (response, status) {
            console.log("Response AddPoster in");
            console.log(response.data);
           //this. 
        });

        // SPRAWDZENIE DO BAZY LOGINU + UTWORZENIE TOKENU
        //$state.go(targetState);
    }
});
angular.module("sbAdminApp")
    .controller("AddPoster", ["$http", "$scope", "AddPosterService", function ($http, $scope, AddPosterService) {
        console.log("AddPoster");
        $http({
            method: "GET",
            url: "/api/GetSections"
        }).then(function (response) {
            $scope.sections = [];
            $scope.sections = response.data;
        });
        var vm = this;
        vm.poster = {};

        vm.addPoster = function () {
            console.log("addposter call");
            console.log(vm.poster.TransportDate);
            vm.poster.TransportDate = moment(new Date(vm.poster.TransportDate)).format("HH:mm DD-MM-YYYY");
            AddPosterService.addPoster(vm.poster);
        }
        //$scope.addPoster = function (poster) {
        //    console.log(poster);
        //    var newDate = moment(new Date(poster.data)).format("HH:mm DD-MM-YYYY");
        //    console.log(newDate);
          
        //}
       



    }]);
