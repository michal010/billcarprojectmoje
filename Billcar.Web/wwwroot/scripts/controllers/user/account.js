'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
    .controller("AccountSettings", ['$http', '$rootScope', '$scope', function ($http, $rootScope,$scope) {
        console.log("AccountSettings");
        $http({
            method: "GET", url: "/api/GetSections", dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            console.log("/api/GetSecions OK");
            $scope.sections = [];
            $scope.sections = response.data;
            $scope.loadingSection = true;
        }, function errorCallback(response) {
            console.log("ERROR response:");
            console.log(response);
        });


        $scope.changePassword = function (user) {
            $http({
                method: "POST",
                url: "/api/ChangeUserPassword",
                dataType: 'json',
                headers: {
                    "Content-Type": "application/json"
                },
                data: { oldPassword: user.OldPassword, newPassword: user.NewPassword }
            }).then(function(response) {
                    console.log("OK");

                    console.log(response.data);

                },
                function errorCallback(response) {
                    console.log("ERROR");
                    console.log(response);
                });
        }
        $scope.changeSection = function (user) {
            
            $http({
                method: "POST",url: "/api/ChangeUserSection",dataType: 'json',
                headers: {"Content-Type": "application/json"},
                data: { section: user.Section.Id }
            }).then(function (response) {
                    console.log("OK");
                    console.log(response.data);
                    console.log(user);
                    $rootScope.userSection = user.Section.CityName;
                    
                },
                function errorCallback(response) {
                    console.log("ERROR");
                    console.log(response);
                });
        }
        //$http({
        //    method: "GET", url: "/api/GetPostersPerson", dataType: 'json',
        //    headers: {
        //        "Content-Type": "application/json"
        //    },
        //    params: user
        //}).then(function (response, status) {
        //    $scope.userPosters = response.data;
        //}, function errorCallback(response) {
        //    console.log("ERROR");
        //    console.log(response);
        //});
    }]);
