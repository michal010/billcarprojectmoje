"use strict";
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module("sbAdminApp")
    .controller("ManageSections", ["$http", "$scope", "$window", function($http, $scope, $window) {
        console.log("ManageSections");
        var removedSections = [];
        $http({
                method: "GET",
                url: "/api/GetSections"
            }).then(function(response) {
                console.log("OK repsonse GetSections");
                $scope.sections = [];
                $scope.sections = response.data;
            }),
            $scope.removeSection = function(Id) {
                $scope.notChanges = 1;
                for (var i in $scope.sections) {
                    if ($scope.sections.hasOwnProperty(i)) {
                        if ($scope.sections[i].Id === Id) {
                            removedSections.push($scope.sections[i].Id);
                            $scope.sections[i].removed = 1;
                            //$scope.people.splice([i], 1);
                        }
                    }
                }
            };
        $scope.addSection = function(name) {
            $scope.notChanges = 1;
            $http({
                method: "POST",
                url: "/api/AddSection",
                dataType: "json",
                headers: {
                    "Content-Type": "application/json"
                },
                data: { sectionName: name }
            }).then(function(response) {

                if (response.data != null && response.data !== undefined) {
                    console.log("Added " + name);
                    $scope.sections.push(response.data);
                }
            });
        };

        $scope.saveChanges = function() {
            console.log("Removing section id...");

            console.log("... : " + removedSections);
            $http({
                method: "POST",
                url: "/api/RemoveSections",
                dataType: "json",
                headers: { "Content-Type": "application/json" },
                data: { ids: removedSections }
            }).then(function(response) {
                console.log("OK remowed " + removedSections);
                console.log(response.data);
                //if (response.data.length == 0) {
                //    for (var i = $scope.sections.length - 1; i >= 0; i--) {
                //        if ($scope.sections[i].removed) {
                //            $scope.sections.splice([i], 1);
                //        }
                //    }
                //} else {
                for (var x = response.data.length - 1; x >= 0 || (x == -1 && response.data.length == 0) ; x--) {
                    for (var i = $scope.sections.length - 1; i >= 0 ; i--) {
                        if (response.data.length!=0 && response.data[x].indexOf("#" + $scope.sections[i].Id + ":") != -1) {
                            if ($scope.sections[i].removed) {
                                $scope.sections[i].removed = false;
                            }
                        } else {
                            if ($scope.sections[i].removed) {
                                $scope.sections.splice([i], 1);
                            }
                        }
                    }
                }
                //}
                   
                    $scope.errorMessage = response.data;
                //}
                removedSections = [];
                $scope.notChanges = false;
            });
        }



    //$http({
    //    method: "GET",
    //    url: "/api/GetUsers",
    //}).then(function (response, status) {
    //    console.log("OK repsonse GetPeople");
    //    $scope.people = [];
    //    $scope.people = response.data;
    //    console.log(response.data);
    //}, function errorCallback(response) {
    //    console.log("ERROR");
    //    console.log(response);
    //});
    //$scope.removeUser = function (name) {
    //    for (var i in $scope.people) {
    //        if ($scope.people[i].name == name)
    //            $scope.people.splice([i], 1);
    //    }
    //}

}]);
