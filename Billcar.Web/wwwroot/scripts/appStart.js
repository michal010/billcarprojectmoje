
app.service("Authorization", function ($http, $state, $cookies, authService, $window) {
        this.authorized = false;
    var existingTokenCheck = function () {
            if ($cookies.get("BillcarToken") == undefined) {
                console.log("No token so create");
                var token = Math.random().toString(36).substr(2);
                $cookies.put("BillcarToken", token + token);
            };
            var tokenCookie = $cookies.get("BillcarToken");
            $http.defaults.headers.common.Authorization = tokenCookie;
    },
        giveToken = function () {
            return $cookies.get("BillcarToken");
        },
        //giveEmail = function () {
        //    this.userEmailAddress = userForm.Email;
        //},
        login = function (userForm) {
                var tokenSend = {
                    TokenValue: $cookies.get("BillcarToken")
                }
                return $http({
                    method: "POST", url: "/api/CheckLogin", dataType: "json",
                    headers: { "Content-Type": "application/json" },
                    data: { user: userForm, token: tokenSend }
                }).then(function (response) {
                    console.log("Response login in");
                    console.log(response.data.Successful);
                    if (response.data.Successful == true) {
                        console.log("Login ok");
                        authService.loginConfirmed();
                        window.authorized = true;
                        $state.go("app.dashboard.addPoster");
                        $window.localStorage.setItem('BillcarEmail', userForm.Email);
                        
                    }
                    return response.data;
                });

                // SPRAWDZENIE DO BAZY LOGINU + UTWORZENIE TOKENU
                //$state.go(targetState);
        },
           
            checkState = function () {
                if (authorized == false && $state.params.authorization == undefined) {
                   $state.go("app.login");
                 }
                if (authorized && $state.params.authorization == false) {
                    $state.go("app.dashboard.addPoster", {}, { reload: true });
                    console.log("XXx");
                }
            },
            logout = function () {
                console.log("logout");
                $http({
                    method: "POST", url: "/api/Logout", dataType: "json",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: { token: { TokenValue: $cookies.get("BillcarToken") } }
                }).then(function (response, status) {
                    console.log("SUCCESS___LOGOUT___v");
                    console.log(response.data);
                    window.authorized = false;
                    authService.loginCancelled();
                    $state.go("app.login", {}, { reload: true });
                    console.log("SUCCESS--LOGOUT----^");
                });
            },
            check = function () {
                $http({
                    method: "POST", url: "/api/CheckIfLogin", dataType: "json",
                    headers: { "Content-Type": "application/json" },
                    data: { token: { TokenValue: $cookies.get("BillcarToken") } }
                }).then(function (response, status) {
                    if (response.data === "True") {
                        authService.loginConfirmed();
                        window.authorized = true;
                    }
                    else {
                        window.authorized = false;
                        authService.loginCancelled();

                    }
                    checkState();
                }, function errorCallback(response) {
                    console.log("ERROR");
                    console.log(response);
                });
            }


    return {
        authorized: this.authorized,
        existingTokenCheck: existingTokenCheck,
        logout: logout,
        login: login,
        check: check,
        checkState: checkState,
        giveToken:giveToken

};
    })
    .run(function ($rootScope, $state, $stateParams, Authorization, $window, $http) {
        console.log("App running");
      
      
        


        
        
        $rootScope.$on("animStart", function ($event, element, speed) {
            console.log("START");
            //angular.element('#animView').removeClass("animContent");
            //angular.element('#animView').addClass('smoothTransform');
          
        });
        
        $rootScope.$on("animMiddle", function ($event, element, speed) {
            //angular.element(element).css('position', 'unset');
            
            $("#page-wrapper").css("min-height", ($("#animView").height()) + "px");
            angular.element('#animView').addClass("animContent");
            console.log("MIDDLE");

        });
        $rootScope.$on("animEnd", function ($event, element, speed) {
            $rootScope.show.loading = 0;
            console.log("END");

        });








        $rootScope.$on("event:auth-loginRequired", function (response, status) {
            if (status.statusText == "Unauthorized") {
               // $window.location.href = "/#/login";
                //window.location.reload();
            }
            if (status.statusText == "No Access") {
                // GoBack
            }
            console.log("You must login!");
        });
        $rootScope.$on("event:auth-loginConfirmed", function () {
            console.log("Ok, You are logged...");
            $http({
                method: "POST", url: "/api/GetUserByToken", dataType: "json",
                headers: { "Content-Type": "application/json" },
            }).then(function (response) {
                $rootScope.userSection = response.data.Section1.CityName;
                $rootScope.userEmail = response.data.Email;
                $rootScope.userName = response.data.FirstName + " "+response.data.LastName;
            });
        });


        
       




        //$scope.logout = function () {
        //    console.log("logout");
        //    $http({
        //        method: "POST", url: "/Home/Logout", dataType: 'json',
        //        headers: {
        //            "Content-Type": "application/json",
        //            "Authorization": $cookies.get("BillcarToken")
        //        },
        //        //ignoreAuthModule: true,
        //        ignoreLoadingBar: true,
        //        data: { token: { TokenValue: $cookies.get("BillcarToken") } }
        //    }).then(function (response, status) {
        //        console.log("SUCCESS___LOGOUT___v");
        //        console.log(response.data);

        //        authService.loginCancelled();

        //        console.log("SUCCESS--LOGOUT----^");
        //    });
        //    //$cookies.remove("billcarAppAuth");
        //    //$cookies.remove("billcarAppUsername");
        //    //$window.location.reload(true);
        //    //$window.location.href = '/#!/login';

        //};






























        //$http({
        //    method: "GET", url: "/api/GetUsers", dataType: 'json',
        //    headers: {
        //        "Content-Type": "application/json",
        //    },
        //}).then(function (response, status) {
        //    console.log(response.data[0]);
        //});



        //$http({
        //    method: "GET", url: "/api/GetUsers",
        //    dataType: 'json',
        //    headers: {
        //        "Content-Type": "application/json"
        //    }
        //}).then(function (response, status) {
        //    console.log("OK");
        //    console.log(response.data[0]);
        //}, function (response) {
        //    console.log("NOT OK");
        //    console.log(response);
        //});
        // UTWORZYC CIASTECZKO TUTAJ


        // LOGOUT
        //Authorization.clear(); 
        //$state.go('home');

        $rootScope.$on("$viewContentLoaded", function (event, toState, toParams, fromState, fromParams) {
            // DODAC HTTP - AUTORYZOWANY - NIE PRZEKIERUJE


        });
    });


