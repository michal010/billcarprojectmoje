'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
var MAIN_FOLDER = '../wwwroot/';
var app = angular.module('sbAdminApp', [
    'http-auth-interceptor',
    'ngMessages',
    'oc.lazyLoad',
    'ui.router',
    'ngCookies',
    'angular-loading-bar',
    'ui.bootstrap',
    'moment-picker',
    'cfp.loadingBar',
    'ngAnimate'
    
]);
// UNIQUE ng-option
app.filter('unique', function () {
    return function (collection, primaryKey) { //no need for secondary key
        var output = [],
            keys = [];
        var splitKeys = primaryKey.split('.'); //split by period


        angular.forEach(collection, function (item) {
            var key = {};
            angular.copy(item, key);
            for (var i = 0; i < splitKeys.length; i++) {
                key = key[splitKeys[i]];    //the beauty of loosely typed js :)
            }

            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});

app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    //cfpLoadingBarProvider.includeBar = false;
    cfpLoadingBarProvider.includeSpinner = false;
    //cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    //cfpLoadingBarProvider.spinnerTemplate = '<div id="page-loading"><img src="/Images/loading.gif" alt="Loading..." id="loading-bar-circle" /> </div>';
    
    cfpLoadingBarProvider.latencyThreshold = 200;
}]);
app.config(['momentPickerProvider', function (momentPickerProvider) {
    momentPickerProvider.options({
        /* Picker properties */
        locale:        'en',
        format:        'L LTS',
        minView:       'decade',
        maxView:       'minute',
        startView:     'year',
        autoclose:     true,
        today:         false,
        keyboard:      false,
            
        /* Extra: Views properties */
        leftArrow:     '&larr;',
        rightArrow:    '&rarr;',
        yearsFormat:   'YYYY',
        monthsFormat:  'MMM',
        daysFormat:    'D',
        hoursFormat:   'HH:[00]',
        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
        secondsFormat: 'ss',
        minutesStep:   5,
        secondsStep:   1
    });
}]);
app.controller("AppController",
    [
        "$rootScope", "$scope", "$state", '$transitions', "Authorization", "cfpLoadingBar", function ($rootScope, $scope, $state, $transitions, Authorization, cfpLoadingBar) {
            console.log("App controller");

            $scope.show = {};
            
            
            $scope.view = {};
            $transitions.onStart({ to: "**" }, function (trans) {
                console.log("STARTTTTTTTTTTT");
                $scope.loadedPage = false;
                //$("#animView").css("width", window.innerWidth + "px");
                //$("#page-wrapper").css("min-width", ($("#animView").height()) + "px");
            });
            $transitions.onBefore({ to: "**" }, function (trans) {
                //angular.element("#animView").addClass("smoothTransform");
                console.log("TWOOO");
                
            });
            
            $transitions.onSuccess({}, function ($transitions) {
                Authorization.existingTokenCheck();
                Authorization.check();
                console.log("STARTTTTTTTTTTT");
                setTimeout(function () {
                    angular.element('#main').removeClass("animIt");
                    
                    console.log("REMOVED");
                }, 25);
                $scope.show.loading = 1;

                if (($transitions.from().name.includes('supervisor') && $transitions.to().name.includes('dashboard')) ||
                    ($transitions.from().name.includes('dashboard') && $transitions.to().name.includes('supervisor')))
               { 
                    angular.element('.headerIcons').addClass("animContent");
                    $scope.view.headers = 1;
                    angular.element('#main').addClass('hideForWhile');
                    setTimeout(function () {
                        angular.element('#main').removeClass("hideForWhile");
                        angular.element('#main').addClass('animIt');
                    }, 100);
                }
                else
                    {
                    $scope.view.headers = 0;
                    }
                if ($state.$current.name.includes('supervisor')) {
                    console.log("ADMIN!");
                    console.log($scope.view);
                    $scope.view.displayHeader = 2;
                    console.log($scope.view);
                    
                } else {
                    console.log("NO ADMIN!");
                    
                    $scope.view.displayHeader = 1;
                    
                }
                
            });
            $scope.$on("cfpLoadingBar:loading", function () {
                var holdWatch = 0;
                setTimeout(function () {
                    holdWatch = cfpLoadingBar.status();
                    if (cfpLoadingBar.status() > 0) {
                        $scope.loadedPage = false;
                    }
                }, 5);
                setTimeout(function () {
                    console.log(cfpLoadingBar.status());
                    if (cfpLoadingBar.status() == holdWatch) {
                        $scope.loadedPage = true;
                    }
                }, 1000);
                        
            });
            $scope.$on("cfpLoadingBar:completed", function () {
                
                setTimeout(function () {
                    if (cfpLoadingBar.status() > 0) {
                        console.log("COMPLETED LOADING");
                        $scope.loadedPage = true;
                    }
                }, 5);
                
                console.log($scope.loadedPage);
            });
           
            
        }
    ]);