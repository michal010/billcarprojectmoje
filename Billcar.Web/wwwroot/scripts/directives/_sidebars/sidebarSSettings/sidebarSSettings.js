'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
  .directive('sidebarSSettings', ['$location', function () {
    return {
        templateUrl: MAIN_FOLDER + 'scripts/directives/_sidebars/sidebarSSettings/sidebarSSettings.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      link: function(scope, element, attrs) {
          var observer = new MutationObserver(function(mutations) {
              // your code here ...
          });
          observer.observe(element[0], {
              childList: true,
              subtree: true
          });
      },
      controller: function ($scope) {
          
          $scope.selectedMenu = 'Informacje o ogłoszeniach';
        $scope.collapseVar = 0;
        $scope.multiCollapseVar = 0;
        
        $scope.check = function(x){
          
          if(x==$scope.collapseVar)
            $scope.collapseVar = 0;
          else
            $scope.collapseVar = x;
        };
        
        $scope.multiCheck = function(y){
          
          if(y==$scope.multiCollapseVar)
            $scope.multiCollapseVar = 0;
          else
            $scope.multiCollapseVar = y;
        };
      }
    }
  }]);
