"use strict";

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module("sbAdminApp")
    .controller("Controller", ["$rootScope", "$scope", "$stateParams", "$transitions", "$location", "$state", function ($rootScope, $scope, $stateParams, $transitions, $location, $state) {
        //$scope.adminHeader = $state.$current.name.includes('supervisor');
        $scope.isActive = function (viewLocation) {
            var active = ($location.path().includes(viewLocation));
            return active;
        };
        
        console.log("ADMIN MAIN:");
        
        $scope.changeItems = function () {
            console.log("CHANGE");
        };
        $scope.$watch(function () {
            //console.log("X");
            return document.body.innerHTML;
        }, function () {
            //console.log("z");
            //TODO: write code here, slit wrists, etc. etc.
        });
        //$rootScope.$on('$viewContentLoaded', function () {
        //    // Code you want executed every time view is opened
        //    console.log('Opened!');
        //    console.log(angular.element().hasClass('ng-hide'));
        //})
        //$scope.$watch(function () { return angular.element('.sidebar-nav .navbar-collapse').is(':visible') }, function () {
        //    console.log("VISIBLE");
        //    console.log(angular.element('.sidebar-nav .navbar-collapse').class);
        //});
        //$scope.$watch(function () { return angular.element.attr('class'); }, function (newValue, oldValue) { console.log(oldValue) });

        //$scope.$watch(function () {
        //    console.log(angular.element('.sidebar-nav .navbar-collapse'));
        //});
        var manageSidebar = function () {
            if ($stateParams.menu != undefined) {
                switch ($stateParams.menu) {
                    case "hidden":
                        $rootScope.menu = 0;
                        break;
                    case "settings":
                        $rootScope.menu = 2;
                        break;
                }
            }
            else {
                $rootScope.menu = 1;
            }
        };
        // Change state - load correct sidebar
        $transitions.onSuccess({}, function() { manageSidebar() });
        // Enter page [reload example] - define menu
        manageSidebar();
        
        
    }])
	.directive("headerSupervisor", function () {
	    return {
	        templateUrl: MAIN_FOLDER + "scripts/directives/_headers/headerSupervisor/headerSupervisor.html",
	        restrict: "E",
	        replace: true
	        
	    }
	});
    


