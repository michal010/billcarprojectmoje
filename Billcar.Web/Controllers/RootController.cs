﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using Billcar.Core.Models;
using Billcar.BL;

namespace WebTest.Controllers
{
    
    
    public partial class RootController : Controller
    {

        // GET: Root
        public ActionResult Index()
        {
            return View();
        }
        
    }
}