﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using Billcar.Core.Models;
using Billcar.BL;

namespace WebTest.Controllers
{
    
    static class JSONConvert
    {
        public static string SerializeObject(object objectItem)
        {
            return JsonConvert.SerializeObject(objectItem, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }



    }

    [RoutePrefix("api")]
    public partial class RootController
    {

        
        [Route("test")]
        [HttpGet]
        public bool Testing()
        {
            return true;
        }
       



        [Route("GetSections")]
        [HttpGet]
        public string GetSections()
        {
            AuthBL connect = new AuthBL();
            List<Section> sections = connect.GetSections();
            return JSONConvert.SerializeObject(sections);
        }
        [RoleSupervisor]
        [Route("RemoveSections")]
        [HttpPost]
        public string RemoveSections(int[] ids)
        {
            List<string> errorMessage = new List<string>();
            SectionBL connect = new SectionBL();
            for (int i = 0; i < ids.Length; i++)
            {
                Section section = connect.Read(ids[i]);
                ValidationResponse response = connect.Delete(section);
                if (response.Information != null && response.Information != "")
                {
                    errorMessage.Add("#" + section.Id + ":" + response.Information);
                }

            }
            return JSONConvert.SerializeObject(errorMessage);
        }
        [Route("EditSections")]
        [HttpPost]
        public bool EditSections(Section[] sections)
        {
            if (sections == null)
                return true;
            SectionBL connect = new SectionBL();
            foreach (Section section in sections)
            {
                connect.Update(section);
            }
            return true;
        }
        [Route("AddSection")]
        [HttpPost]
        public string AddSection(string sectionName)
        {
            if (string.IsNullOrEmpty(sectionName))
                return null;
            SectionBL connect = new SectionBL();
            Section add = new Section
            {
                CityName = sectionName
            };
            connect.Create(add);
            return JSONConvert.SerializeObject(add);
        }




        [Route("GetUsers")]
        [HttpGet]
        public string GetUsers()
        {
            PersonBL connect = new PersonBL();
            List<Person> people = connect.GetPeople();
            return JSONConvert.SerializeObject(people);
        }
        [Route("RemoveUser")]
        [HttpPost]
        public bool RemoveUsers(int[] ids)
        {
            if (ids == null)
                return true;
            PersonBL connect = new PersonBL();
            for(int i=0; i<ids.Length;i++)
            {
                Person man = connect.Read(ids[i]);
                connect.Delete(man);
            }
            return true;
        }
        [Route("EditUser")]
        [HttpPost]
        public bool EditUser(Person[] users)
        {
            if (users == null)
                return true;
            PersonBL connect = new PersonBL();
            foreach (Person user in users)
            {
             connect.Update(user);
            }
            return true;
        }
        
        [Route("GetPosters")]
        [HttpGet]
        public string GetPosters()
        {
            PosterBL connect = new PosterBL();
            List<Poster> posters = connect.GetPosters();
            var result = JSONConvert.SerializeObject(posters);
            return result;
        }
        [Route("GetPostersCount")]
        [HttpGet]
        public int GetPostersCount()
        {
            PosterBL connect = new PosterBL();
            return connect.GetPosters().Count;
        }

        //[HttpGet]
        //public int GetPersonID(Person p)
        //{
        //    PosterBL connect = new PosterBL();
        //    PersonBL connectPerson = new PersonBL();
        //    foreach (Person man in connectPerson.GetPeople())
        //        if (man.Id == p.Id)
        //            p = man;
        //    return p.Id;
        //}
        [Route("GetPostersPerson")]
        [HttpGet]
        public int GetPostersPerson(Person p)
        {
            PosterBL connect = new PosterBL();
            PersonBL connectPerson = new PersonBL();
            foreach (Person man in connectPerson.GetPeople())
                if (man.Id == p.Id)
                    p = man;
            return connect.GetPostersByPerson(p).Count;
        }

        [Route("CheckLogin")]
        [HttpPost]
        public string CheckLogin(Person user, Token token)
        {
            AuthBL connect = new AuthBL();
            Person person;
            ValidationResponse response = new ValidationResponse();

            person = connect.UserLogIn(user);
            if (person != null)
            {
                if (person.Id!=0)
                {
                    token.Person = person.Id;
                    if (connect.CheckToken(token) == false)
                    {
                        response = connect.createToken(token)
                            ? new ValidationResponse(true)
                            : new ValidationResponse(false, "Błąd wewnętrzny.");
                    }
                }
                else
                {
                    response = new ValidationResponse(false, "Złe hasło.");
                }
            }
            else
            {
                response = new ValidationResponse(false, "Dany użytkownik nie istnieje.");
            }
            return JSONConvert.SerializeObject(response);

            //List<Section> people = connect.GetSections();
            //var Tuser = user;

            //var JsonResult = Json(people, JsonRequestBehavior.AllowGet);
            //JsonResult.MaxJsonLength = int.MaxValue;
            //return JsonResult;
        }
        [Route("RegisterLogin")]
        [HttpPost]
        public string RegisterLogin(Person user)
        {
            AuthBL connect = new AuthBL();
            ValidationResponse exist = connect.IfUserExist(user);
            ValidationResponse response = exist.Successful ? new ValidationResponse(false) : connect.UserRegister(user); 
            return JSONConvert.SerializeObject(response); 
            //return new ValidationResponse(true); 


            //List<Section> people = connect.GetSections();

            //var JsonResult = Json(people, JsonRequestBehavior.AllowGet);
            //JsonResult.MaxJsonLength = int.MaxValue;
            //return JsonResult;

        }
        [Route("Logout")]
        [HttpPost]
        public bool Logout(Token token)
        {
            AuthBL auth = new AuthBL();
            bool result = auth.RemoveToken(token);
            return result;
        }

        [Route("CheckIfLogin")]
        [HttpPost]
        public bool CheckIfLogin(Token token)
        {
            AuthBL auth = new AuthBL();
            bool result = auth.CheckToken(token);
           
            return result;
        }


        [Route("AddPoster")]
        [HttpPost]
        public bool AddPoster(Poster poster)
        {
            poster.AuthorID = 12;
            poster.TransportMaxSize = 7;
            PosterBL connect = new PosterBL();
            bool result = connect.Create(poster);
            return result;
        }


        [Route("PostersCountInfo")]
        [HttpPost]
        [AuthorizationFilter]
        public string PostersCountInfo()
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connectP = new PosterBL();
            List<Poster> posters = connectP.GetPosters();

            int[] values = new int[3];
            values[0] = posters.Count;
            values[1] = posters.Count(o => o.AuthorID == person.Id);
            values[2] = person.Poster.Count;

            var result = JSONConvert.SerializeObject(values);
            return result;
        }
        [Route("GetUserByToken")]
        [HttpPost]
        [AuthorizationFilter]
        public string GetUserByToken()
        {
            Token token = ReturnTokenObject();
            AuthBL connect = new AuthBL();
            if (token == null) return "Error";
            Token t = connect.returnToken(token);
            return JSONConvert.SerializeObject(t.Person1);
        }
        [Route("ChangeUserPassword")]
        [HttpPost]
        [AuthorizationFilter]
        public bool ChangeUserPassword(string oldPassword, string newPassword)
        {
            SecurityBL security = new SecurityBL();
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PersonBL connect = new PersonBL();
            string salt = person.Salt;
            bool result;

            oldPassword = security.GenerateSHA256Hash(oldPassword, salt);

            if (oldPassword != person.Password)
            {
                return false;
            }
            salt = security.CreateSalt(32);
            newPassword = security.GenerateSHA256Hash(newPassword, salt);
            person.Password = newPassword;


            result = connect.Update(person);

            return result;
        }
        [Route("ChangeUserSection")]
        [HttpPost]
        [AuthorizationFilter]
        public bool ChangeUserSection(int section)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PersonBL connect = new PersonBL();

            person.Section = section;

            return connect.Update(person);
        }
        private Token ReturnTokenObject()
        {
            Token token = new Token()
            {
                TokenValue = Request.Headers.GetValues("Authorization")[0]
            };
            return token;
        }
    }
}